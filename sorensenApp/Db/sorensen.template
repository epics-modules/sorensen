
# EPICS template for Sorensen SGA30x501D
# Used for ESS LEBT solenoids 
# together with sorensen.proto StreamDevice protocol file

## Voltage
record(ao, "${P}${R}Vol-S") {
  field(DESC, "Set Voltage")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} set_voltage ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "V")
  field(HOPR, "30")
  field(LOPR, "0")
  field(DRVL,"${MINVOL=0}")
  field(DRVL,"${MAXVOL=0}")
  #field(VAL,"13")
  #field(PINI,"TRUE")
  field(TPRO, "1")
  info(autosaveFields, "PREC VAL")
}

record(ai, "${P}${R}Vol-RB") {
  field(DESC, "Get Voltage setpoint")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} get_voltage ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "V")
  field(HOPR, "30")
  field(LOPR, "0")
}

record(ai, "${P}${R}Vol-R") {
  field(DESC, "Measure Voltage")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} measure_voltage ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "V")
  field(HOPR, "30")
  field(LOPR, "0")
}

#record(stringin, "${P}${R}SeqStat-R") {
#  field(DESC, "Sequence Status")
#  field(SCAN, "1 second")
#  field(DTYP, "stream")
#  field(INP, "@${proto_file} get_seqstate ${Ch_name}")
#  #field(PREC, "3")
#  #field(LINR, "NO CONVERSION")
#  #field(EGU, "V")
#  #field(HOPR, "30")
#  #field(LOPR, "0")
#}

## Current
record(ao, "${P}${R}CurImm-S") {
  field(DESC, "Set Current - Immediate Function")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} set_current ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "A")
  field(HOPR, "${MAXCUR=500}")
  field(LOPR, "${MINCUR=0}")
  field(DRVL,"${MINCUR=0}")
  field(DRVL,"${MAXCUR=0}")
  field(TPRO, "1")
  info(autosaveFields, "PREC VAL")
}

#-------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------

## Current
record(ao, "${P}${R}Cur-S") {
  field(DESC, "Set Current - Origin  ")
  field(VAL,"0")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "A")
  field(HOPR, "${MAXCUR=500}")
  field(LOPR, "${MINCUR=0}")
  field(DRVL,"${MINCUR=0}")
  field(DRVL,"${MAXCUR=0}")
  info(autosaveFields, "PREC VAL")
  field(TPRO, "0")
  field(FLNK, "${P}${R}CurCondition")
}

record(calcout, "$(P)$(R)CurCondition") {
   
   field(INPA, "$(P)$(R)Cur-S ")
   field(INPB, "$(P)$(R)Cur-RB ")
   field(INPC, "$(P)$(R)RampSpeed-S CPP")
   field(INPD, "$(P)$(R)RampType-S CPP")     # Immediate=0, Ramp=1
   
   field(CALC, "(ABS(B-A)>C) && D ? 2 : 1")  # Ramp = 2 = 10 , Immediate = 1 = 01

   field(TPRO, "0")
   field(OUT,  "$(P)$(R)CurSelection PP")
}

# select is either 10 to Ramp or 01 to Immediate
record(mbbo, "$(P)$(R)CurSelection") {
   field(VAL, "2")
   field(TPRO, "0")
   field(FLNK, "$(P)$(R)CurDelivery.PROC")
}

record(seq, "$(P)$(R)CurDelivery") {
  
   field(SELM, "Mask")                  # selection mechanism
   field(SELL, "$(P)$(R)CurSelection")  # location of mask
   field(SHFT, "0")                     # MUST be set to zero

   field(DOL0, "$(P)$(R)Cur-S")                  
   field(DOL1, "$(P)$(R)Cur-S")                  
      
   field(LNK0,"$(P)$(R)CurImm-S  PP")   # Immediate
   field(LNK1,"$(P)$(R)CurRamp-S PP")	# Ramp
}


## Current
record(ao, "${P}${R}CurRamp-S") {
  field(DESC, "Set Current - Ramp Function")
  #field(DTYP, "stream")
  #field(OUT, "@${proto_file} set_current_ramp ${Ch_name}")
  field(VAL,"0")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "A")
  field(HOPR, "${MAXCUR=500}")
  field(LOPR, "${MINCUR=0}")
  field(DRVL,"${MINCUR=0}")
  field(DRVL,"${MAXCUR=0}")
  info(autosaveFields, "PREC VAL")
  field(TPRO, "0")
  field(FLNK, "${P}${R}RampCalcSec-S")
}

record(stringout, "${P}${R}TrigRamp-S") {
  field(DESC, "Trigger Ramp")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} trig_ramp ${Ch_name}")
}

record(stringout, "${P}${R}TrigAbort-S") {
  field(DESC, "Trigger Abort")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} trig_abort ${Ch_name}")
}

record(ao, "${P}${R}RampSpeed-S") {
  field(DESC, "RampSpeed")
  field(PREC, "0")
  field(LINR, "NO CONVERSION")
  field(EGU, "A/s")
  field(DRVH, "10")
  field(DRVL, "1")
  field(HOPR, "10")
  field(LOPR, "1")
  field(VAL, "1")
}


record(calcout, "${P}${R}SetCurRampData-S") {
  field(DESC, "Trigger Ramp EV")
  field(DTYP, "stream")
  field(INPA, "${P}${R}CurRamp-S")
  field(INPB, "${P}${R}RampCalcSec-S")
  field(OUT, "@${proto_file} set_current_ramp_data(INPA,INPB) ${Ch_name}")
  field(CALC, "0")
  field(OOPT, "Every Time")
  field(TPRO, "0")
  field(FLNK, "${P}${R}TrigRamp-S")
}

record(calc, "${P}${R}RampCalcSec-S") {
  field(DESC, "Calc Num Seconds Ramp reaches Target")
  field(INPA, "${P}${R}Cur-R")
  field(INPB, "${P}${R}CurRamp-S")
  field(INPC, "${P}${R}RampSpeed-S")
  field(CALC, "ABS((B-A)/C)")
  #field(OOPT, "Every Time")
  field(TPRO, "0")
  field(FLNK, "${P}${R}SetCurRampData-S")
}


record(mbbo,"$(P)$(R)Abort-Process") {
 field(DTYP,"Raw Soft Channel")
 field(FLNK,"$(P)$(R)Abort-ProcessSQ.VAL PP NMS")
 field(VAL,"0")
 field(TPRO,"1")
}

record(seq,"$(P)$(R)Abort-ProcessSQ") {
 field(SELM,"All")
 
 field(DLY1,"0") # Don't wait
 field(LNK1,"$(P)$(R)TrigAbort-S PP NMS")

# We use CurImm-S to assign the actual  Cur-R so the Ramp Function works again.
 field(DLY2,"1") # Wait for a Second
 field(DOL2,"$(P)$(R)Cur-R.VAL PP NMS") # Assign CurImm-S Value to allow ramp function to re-work
 field(LNK2,"$(P)$(R)CurImm-S.VAL PP NMS")
}

record(bi, "${P}${R}RampStatus-R") {
  field(DESC, "Ramp Status")
  field(DTYP, "stream")
  field(SCAN, "1 second")
  field(INP, "@${proto_file} get_ramp_status ${Ch_name}")
  field(ZNAM, "Completed")
  field(ONAM, "Progress")
}


record(mbbo, "$(P)$(R)RampType-S") {
  field(DESC, "Set ramp type")
  field(ZRST, "Immediate")
  field(ONST, "Ramp")
  field(VAL,"1")
  field(PINI, "TRUE")
}


#-------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------

record(ai, "${P}${R}Cur-RB") {
  field(DESC, "Get current setpoint")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} get_current ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "A")
}

record(ai, "${P}${R}Cur-R") {
  field(DESC, "Measure Current")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} measure_current ${Ch_name}")
  field(PREC, "3")
  field(LINR, "NO CONVERSION")
  field(EGU, "A")
  field(HOPR, "${MAXCUR=500}")
  field(LOPR, "${MINCUR=0}")
  field(FLNK, "${P}${R}Fld-RB")
}

## Magnetic Field
record(ai, "${P}${R}Fld-RB") {
  field(DESC, "Magnetic Field")
  field(SCAN, "Passive")
  field(PREC, "6")
  field(LINR, "SLOPE")
  field(EGU, "T")
  field(ESLO, "${Amp2Tesla_calib}")
  field(DTYP, "Raw Soft Channel")
  field(INP, "${P}${R}Cur-R")
}

record(ao, "${P}${R}Fld-S") {
  field(DESC, "Set Magnetic Field")
  field(PREC, "6")
  field(LINR, "NO CONVERSION")
  field(EGU, "T")
  field(HOPR, "${FLD_HOPR=0}")
  field(LOPR, "${FLD_LOPR=0}")
  field(FLNK, "${P}${R}CalcCur PP")
  info(autosaveFields, "PREC VAL")
}

record(calcout, "${P}${R}CalcCur") {
  field(DESC, "Calc current from Mag Field")
  field(PREC, "6")
  field(EGU, "A")
  field(INPA, "${P}${R}Fld-S NPP")
  field(CALC, "A*(1/${Amp2Tesla_calib})")
  #field(OUT, "${P}${R}CurImm-S PP")
  field(OUT, "${P}${R}Cur-S PP")
  field(OOPT, "Every Time")
}

record(calcout, "${P}${R}CalcFld") {
  field(DESC, "Calc field from current")
  field(PREC, "9")
  field(EGU, "T")
  #field(INPA, "${P}${R}CurImm-S CPP")
  field(INPA, "${P}${R}Cur-S CPP")
  field(CALC, "A*${Amp2Tesla_calib}")
  field(OUT, "${P}${R}Fld-S PP")
  field(OOPT, "Every Time")
  field(TPRO, "0")
}

## Reset (power-on reset)
# Note: this command resets max voltage and max current to power-on defaults!
# (see SG Series Programming Manual, Table 5-2) 
#
record(ao, "${P}${R}Rst") {
  field(DESC, "Reset")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} reset ${Ch_name}")
  field(VAL,"0")

}

## Power Supply
record(bo, "${P}${R}Pwr-S") {
  field(DESC, "Power supply command")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} power_cmd ${Ch_name}")
  field(ZNAM, "OFF")
  field(ONAM, "ON")
#  field(FLNK, "${P}${R}AbortTrig")
}

# Launches Abort Command when Pwr-R =0. 
# To avoid The current to go up automatically when switch ON again.
record(calcout, "${P}${R}AbortTrig") {
  field(DESC, "Trigger Abort When Switch OFF PS")
  field(INPA, "${P}${R}Pwr-R CPP")
  field(CALC, "A=0?1:0")
  field(OOPT, "When Non-zero")
  field(OUT, "$(P)$(R)Abort-Process.PROC")
  field(TPRO, "1")
}

record(bi, "${P}${R}Pwr-R") {
  field(DESC, "Get power supply status")
  field(DTYP, "stream")
  field(SCAN, "1 second")
  field(INP, "@${proto_file} get_status ${Ch_name}")
  field(ZNAM, "OFF")
  field(ONAM, "ON")
}

## Local/Remote
record(bo, "${P}${R}Lck-S") {
  field(DESC, "Local Remote Mode selection")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} loc_rem_mode ${Ch_name}")
  field(ZNAM, "LOC")
  field(ONAM, "REM")
}

record(bi, "${P}${R}Lck-R") {
  field(DESC, "Local Remote Mode selection")
  field(DTYP, "stream")
  field(SCAN, "1 second")
  field(INP, "@${proto_file} get_loc_rem_mode ${Ch_name}")
  field(ZNAM, "LOC")
  field(ONAM, "REM")
}

# Security
#
# Note: this record causes a device clear and reset every time the IOC is started!
# (see the protocol file, set_ovp)
#
record(ao, "${P}${R}Ovp-S") {
  field(DESC, "Set Over Voltage Protection")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} set_ovp ${Ch_name}")
  field(HOPR, "30")
  field(LOPR, "0")
  info(autosaveFields, "VAL")
}

#
# added readback for OVP, for consistency
# Could be useful if value is changed locally (even if unlikely)
#

record(ai, "${P}${R}Ovp-R") {
  field(DESC, "Get Over Voltage Protection setting")
  field(DTYP, "stream")
  field(SCAN, "10 second")
  field(INP, "@${proto_file} get_ovp ${Ch_name}")
  field(HOPR, "30")
  field(LOPR, "0")
}

## Errors
record(bi, "${P}${R}OvpErrR") {
  field(DESC, "Get OVP Error flag")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} get_ovp_error ${Ch_name}")
}

record(longin, "${P}${R}NxtErrR") {
  field(SCAN, "1 second")
  field(DESC, "Get the next error")
  field(DTYP, "stream")
  field(INP, "@${proto_file} get_next_error ${Ch_name}")
}

record(aSub, "${P}${R}ErrR") {
  field(SCAN, "1 second")
  field(DESC, "Get error message")
  field(PINI, "YES")
  field(SNAM, "get_error_message")
  field(INPA, "${P}${R}NxtErrR")
  field(FTA, "LONG")
  field(FTVA, "STRING")
}

record(stringout, "${P}${R}ErrClr") {
  field(DESC, "Get the next error")
  field(DTYP, "stream")
  field(OUT, "@${proto_file} clear_error ${Ch_name}")
  field(VAL,"0")

}

record(mbbiDirect, "${P}${R}Status") {
  field(DESC, "Get status byte")
  field(SCAN, "1 second")
  field(DTYP, "stream")
  field(INP, "@${proto_file} get_statusbyte ${Ch_name}")
}
